import sys
import re
import subprocess


MODIFIED_FILE_RE = re.compile('^(?:M|A)(\s+)(?P<name>.*)')
RED = '\033[31m'
GREEN = '\033[32m'
RESET = '\033[30m'


def run_process(command):
    process = subprocess.Popen(
        command, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE, shell=True)
    out, err = process.communicate()
    return out, err


class CoverageChecker(object):
    COVERAGE_FILE = '.pre-commit.coverage'

    def __init__(self, files):
        self.files = files
        self.files_coverage = {}
        self.has_files_not_coverage = False

    def _run_coverage_report(self):
        command = "$VIRTUAL_ENV/bin/coverage report"
        out, _ = run_process(command)
        return out

    def _parse_coverage_report(self, content):
        lines = content.splitlines()

        for line in lines[1:-1]:
            file_name = line.split()[0]
            self.files_coverage[file_name] = line

        self.coverage_number = lines[-1].split()[-1][:-1]

    def _get_last_coverage_number(self):
        try:
            with open(self.COVERAGE_FILE) as f:
                self.last_coverage_number = f.readlines()[0]
        except:
            self.last_coverage_number = 0

    def _save_coverage_number(self):
        with open(self.COVERAGE_FILE, 'w') as f:
            f.write(self.coverage_number)

    def _print_coverage_for_files(self):
        for file_path in self.files:
            file_without_extension = file_path[:file_path.rindex('.')]

            if file_without_extension in self.files_coverage:
                line = self.files_coverage[file_without_extension]
                coverage_number = int(line.split()[-1][:-1])
                if coverage_number < 100:
                    self.has_files_not_coverage = True
                    print "%s\t%s%s" % (RED, line, RESET)
                else:
                    print "%s\t%s%s" % (GREEN, line, RESET)

    def _print_increase_coverage(self):
        print "---------------------------------------------------------------"
        if self.coverage_number > self.last_coverage_number:
            print "%sIncrease coverage from %s to %s%s" % (
                GREEN, self.last_coverage_number,
                self.coverage_number, RESET)
        elif self.coverage_number < self.last_coverage_number:
            print "%sDecrease coverage from %s to %s%s" % (
                RED, self.last_coverage_number,
                self.coverage_number, RESET)
        else:
            print "%sCoverage stay at %s%s" % (
                GREEN, self.coverage_number, RESET)

    def _prompt_user(self):
        if self.has_files_not_coverage:
            sys.stdin = open('/dev/tty', 'r')
            var = raw_input("You have some files were not coverage. Do you still want to commit? [yes] ")  # NOQA
            if var != "yes":
                sys.exit(1)

    def execute(self):
        self._get_last_coverage_number()

        out = self._run_coverage_report()
        self._parse_coverage_report(out)
        self._print_coverage_for_files()
        self._print_increase_coverage()
        self._save_coverage_number()
        self._prompt_user()


def test_coverage(argv=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('filenames', nargs='*', help='Test and check coverage')
    args = parser.parse_args(argv)

    retval = 0
    CoverageChecker(args.filenames).execute()


if __name__ == '__main__':
    sys.exit(test_coverage())
